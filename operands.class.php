<?php

// Подключение дополнительных классов
require_once __DIR__ . DIRECTORY_SEPARATOR . 'types/number.class.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'types/operands.class.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'types/parenthesis.class.php';

/**
 * Класс-фабрика доступных действий
 */
abstract class Operands {

    // Текущее значение
    protected $value = '';

    /**
     * Конструктор класса
     * @param $value - текущее значение
     */
    public function __construct($value) {
        $this->value = $value;
    }

    /**
     * Определение действия
     * @param $value - действие в строковом определении
     */
    public static function get($value) {
        // Экземпляр фабрики
        if (is_object($value) && $value instanceof Operands)
            return $value;

        // Число
        else if (is_numeric($value))
            return new Number($value);

        // Сложение
        else if ($value == '+')
            return new Addition($value);

        // Вычитание
        else if ($value == '-')
            return new Subtraction($value);

        // Умножение
        else if ($value == '*')
            return new Multiplication($value);

        // Деление
        else if ($value == '/')
            return new Division($value);

        // Возведение в степень
        else if ($value == '^')
            return new Involution($value);

        // Выражение в скобках
        else if (in_array($value, array('(', ')')))
            return new Parenthesis($value);

        // Действие не определено
        throw new Exception('Undefined operand ' . $value);
    }

    /**
     * Базовый метод запуска
     * @param $stack - экземпляр очереди
     */
    abstract public function run(Stack $stack);

    /**
     * Возврат текущего значения
     * @return mixed
     */
    public function render() {
        return $this->value;
    }

    /**
     * Является ли действие оператором
     * @return boolean
     */
    public function isOperand() {
        return false;
    }

    /**
     * Является ли действие "выражение в скобках"
     * @return boolean
     */
    public function isParenthesis() {
        return false;
    }

}