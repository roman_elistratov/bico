<?php

// Подключение дополнительных классов
require_once __DIR__ . DIRECTORY_SEPARATOR . 'calculate.class.php';

// Получение математического выражения из консоли
if (!isset($argv[1]))
    die('Expression is empty');
if (sizeof($argv) > 2)
    die('Expression must be in quotes');

$expression = $argv[1];

// Создание экземпляра калькулятора
$calculate = new Calculate;

// Вычисление выражения
print "Expression: " . $expression . "\n\n";
print "Result: ";
var_dump($calculate->evaluate($expression));

exit(0);