<?php
Header('Content-Type: text/plain');
// Подключение дополнительных классов
require_once __DIR__ . DIRECTORY_SEPARATOR . 'stack.class.php';
require_once __DIR__ . DIRECTORY_SEPARATOR . 'operands.class.php';

/**
 * Класс вычисления математических выражений из строкового представления
 */
class Calculate {

    /**
     * Запуск вычисления
     * @param $string - строковое представления математического выражения
     */
    public function evaluate($string) {
        return $this->run( $this->parse($string) );
    }

    /**
     * Разбор формулы по частям
     * @param $string - строковое представления математического выражения
     * @return Stack
     */
    public function parse($string) {
        // Получение частей выражения
        $tokens = $this->tokenize($string);
        // Инициализация очередей
        $output = new Stack;
        $operands = new Stack;

        // Анализ частей выражения
        foreach ($tokens as $token) {
            // Экземпляр выражения
            $operand = Operands::get($token);

            // Анализ вычисляемого выражения
            if ($operand->isOperand())
                $this->parseOperand($operand, $output, $operands);
            // Анализ "выражения в скобках"
            else if ($operand->isParenthesis())
                $this->parseParenthesis($operand, $output, $operands);
            // Добавление выражения в очередь
            else
                $output->push($operand);
        }

        // Предварительные вычисления
        while ( ( $op = $operands->pop() ) ) {
            if ($op->isParenthesis())
                throw new RuntimeException('Mismatched Parenthesis');
            $output->push($op);
        }

        return $output;
    }

    /**
     * Вычисление математического выражения
     * @param $stack - экземпляр очереди
     * @return mixed
     */
    public function run(Stack $stack) {
        while (($operand = $stack->pop()) && $operand->isOperand()) {
            $value = $operand->run($stack);
            if (!is_null($value))
                $stack->push(Operands::get($value));
        }
        return $operand ? $operand->render() : $this->render($stack);
    }

    /**
     * Анализ "выражения в скобках"
     * @param $operand - экземпляр выражения
     * @param $output - экземпляр очереди
     * @param $operands - экземпляр очереди
     */
    protected function parseParenthesis(Operands $operand, Stack $output, Stack $operands) {
        if ($operand->isOpen())
            $operands->push($operand);
        else {
            $clean = false;
            while ( $end = $operands->pop() ) {
                if ($end->isParenthesis()) {
                    $clean = true;
                    break;
                } else
                    $output->push($end);
            }
            if (!$clean)
                throw new RuntimeException('Mismatched Parenthesis');
        }
    }

    /**
     * Анализ вычисляемого выражения
     * @param $operand - экземпляр выражения
     * @param $output - экземпляр очереди
     * @param $operands - экземпляр очереди
     */
    protected function parseOperand(Operands $operand, Stack $output, Stack $operands) {
        $end = $operands->last();
        if (!$end)
            $operands->push($operand);
        else if ($end->isOperand()) {
            do {
                if ($operand->isShift() && $operand->getPrecedence() <= $end->getPrecedence())
                    $output->push($operands->pop());
                else if (!$operand->isShift() && $operand->getPrecedence() < $end->getPrecedence())
                    $output->push($operands->pop());
                else
                    break;
            } while (($end = $operands->last()) && $end->isOperand());
            $operands->push($operand);
        } else
            $operands->push($operand);
    }

    /**
     * Получение частей математического выражения
     * @param $string - строковое представления математического выражения
     * @return array
     */
    protected function tokenize($string) {
        $parts = preg_split('(([\d\.]+|\+|-|\(|\)|\*|/)|\s+)', $string, null, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
        $parts = array_map('trim', $parts);
        return $parts;
    }
}