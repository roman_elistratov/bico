<?php

/**
 * Класс реализации очереди
 */
class Stack {

    // Содержание очереди
    protected $data = array();

    /**
     * Добавление елемента в очередь
     * @param $element - элемент добавления в очередь
     */
    public function push($element) {
        $this->data[] = $element;
    }

    /**
     * Переход в конец очереди и возврат последнего элемента
     * @return mixed
     */
    public function last() {
        return end($this->data);
    }

    /**
     * Возвращение последнего элемента очереди
     * @return mixed
     */
    public function pop() {
        return array_pop($this->data);
    }

}