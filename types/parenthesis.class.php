<?php

/**
 * Класс обработки "выражение в скобках"
 */
class Parenthesis extends Operands {

    // Приоритет действия
    protected $precedence = 8;

    /**
     * Запуск обработки действия
     * @param $stack - экземпляр очереди
     */
    public function run(Stack $stack) {}

    /**
     * Получение приоритета
     * @return int
     */
    public function getPrecedence() {
        return $this->precedence;
    }

    /**
     * Является ли действие "выражение в скобках"
     * @return boolean
     */
    public function isParenthesis() {
        return true;
    }

    /**
     * Установка текущего значения операции
     * @return string
     */
    public function isOpen() {
        return $this->value == '(';
    }

}