<?php

/**
 * Класс обработки вычисляемых выражений
 */
abstract class Operand extends Operands {

    // Приоритет действия
    protected $precedence = 0;
    // Требуется ли сдвиг в очереди
    protected $shift = true;

    /**
     * Получение приоритета
     * @return int
     */
    public function getPrecedence() {
        return $this->precedence;
    }

    /**
     * Получение необходимости сдвига в очереди
     * @return boolean
     */
    public function isShift() {
        return $this->shift;
    }

    /**
     * Является ли действие оператором
     * @return boolean
     */
    public function isOperand() {
        return true;
    }

}

/**
 * Класс обработки сложения
 */
class Addition extends Operand {

    // Приоритет действия
    protected $precedence = 2;

    /**
     * Запуск обработки действия
     * @param $stack - экземпляр очереди
     */
    public function run(Stack $stack) {
        return $stack->pop()->run($stack) + $stack->pop()->run($stack);
    }

}

/**
 * Класс обработки вычитания
 */
class Subtraction extends Operand {

    // Приоритет действия
    protected $precedence = 2;

    /**
     * Запуск обработки действия
     * @param $stack - экземпляр очереди
     */
    public function run(Stack $stack) {
        $left = $stack->pop()->run($stack);
        $right = $stack->pop()->run($stack);
        return $right - $left;
    }

}

/**
 * Класс обработки умножения
 */
class Multiplication extends Operand {

    // Приоритет действия
    protected $precedence = 4;

    /**
     * Запуск обработки действия
     * @param $stack - экземпляр очереди
     */
    public function run(Stack $stack) {
        return $stack->pop()->run($stack) * $stack->pop()->run($stack);
    }

}

/**
 * Класс обработки деления
 */
class Division extends Operand {

    // Приоритет действия
    protected $precedence = 4;

    /**
     * Запуск обработки действия
     * @param $stack - экземпляр очереди
     */
    public function run(Stack $stack) {
        $left = $stack->pop()->run($stack);
        $right = $stack->pop()->run($stack);

        if ($left == 0)
            throw new RuntimeException('Division by zero');

        return $right / $left;
    }

}

/**
 * Класс обработки возведения в степень
 */
class Involution extends Operand {

    // Приоритет действия
    protected $precedence = 6;

    /**
     * Запуск обработки действия
     * @param $stack - экземпляр очереди
     */
    public function run(Stack $stack) {
        $left = $stack->pop()->run($stack);
        $right = $stack->pop()->run($stack);
        return pow($right, $left);
    }

}