<?php

/**
 * Класс обработки числовых значений
 */
class Number extends Operands {

    /**
     * Запуск обработки действия
     * @param $stack - экземпляр очереди
     */
    public function run(Stack $stack) {
        return $this->value;
    }

}